#!/usr/bin/python 
# -*- coding: UTF-8 -*-

#练习1

#
import turtle
p=turtle.Turtle()
p.ht()
p.up()
p.goto(0,0)
p.down()


#画正方形边框
p.speed(3)
p.color('black')
p.pensize(3)
for i in range(4):
    p.fd(100)
    p.lt(90)

#画内部直线
p.speed(10)
p.pensize(1)
for i in range(1,100,3):
    p.up()
    p.goto(i,0)
    p.down()
    p.goto(100,i)
    
for i in range(1,100,3):
    p.up()
    p.goto(100,i)
    p.down()
    p.goto(100-i,100)
    
    
for i in range(1,100,3):
    p.up()
    p.goto(100-i,100)
    p.down()
    p.goto(0,100-i)

        
for i in range(1,100,3):
    p.up()
    p.goto(0,100-i)
    p.down()
    p.goto(i,0)

#画中心点
p.up()
p.goto(50,50)
p.down()
p.dot('purple')
#画圆
for i in range(16):
    p.circle(10)
    p.rt(22.5)
    
p.hideturtle()
p.done()    
