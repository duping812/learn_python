# 1. Python简介
- Python 由 Guido van Rossum 于 1989 年底发明，第一个公开发行版发行于 1991 年。
- Python 源代码遵循 GPL(GNU General Public License) 协议。
- Python 是目前世界上最受欢迎的编程语言之一。

Python有2个主要版本，2.x和3.x。ROS Melodic及之前版本默认支持2.x，本教程主要以ubuntu 18 下的python 2.7为例。

# 2. Python特点
简单的说，Python是一个“优雅”、“明确”、“简单”的编程语言。
- 学习曲线低，非专业人士也能上手
- 开源系统，拥有强大的生态圈
- 解释型语言，完美的平台可移植性
- 动态类型语言，支持面向对象和函数式编程
- 代码规范程度高，可读性强

Python功能强大，在web开发，网络爬虫，人工智能，数据分析，自动化运维等方面都有很多应用。例如目前很火的人工智能领域，深度学习，神经网络的很多框架都是用python实现的。因为Python有很多库方便做人工智能，比如numpy, scipy做数值计算的，scikit-learn做机器学习的，pybrain做神经网络的，matplotlib将数据可视化的。

# 3. 基本语法
 **1. 代码缩进** 
- Python最具特色的就是用缩进来写模块，不用{}。
- 缩进的空白数量是可变的，但是所有代码块语句必须包含相同的缩进空白数量，这个必须严格执行。
- 建议使用4个空格缩进。
- 空格和Tab不能混用。

一些编辑器，支持自定义tab转空格，以及缩进空格数，例如gedit

![gedit](https://images.gitee.com/uploads/images/2021/1103/115108_6a2a085d_8854469.png "屏幕截图.png")

 **2.行结束符** 
- 如果熟悉其他计算机语言，可能会习惯于每行以分号结束。
- python则不用，可以在每句末尾加上分号，但不会有任何作用。当然如果同一行内有多句代码，则每句之间是需要加上分号用来分割的。


# 4. 示例程序
1. hello world

![hello world](https://images.gitee.com/uploads/images/2021/1103/114831_904581b2_8854469.png "屏幕截图.png")

2. 输入输出 input, print
3. 条件语句 if else

```
a = input("type a number: ")
if a > 0:
    print "%d is positive." %a
elif a == 0:
    print "%d is zero." %a
else:
    print "%d is negative." %a
```


4. 循环语句 for, while

```
for a in range(5, 10):
    print 'a = ', a
    a += 1
```

```
a = 5

while a < 10:
    print 'a = ', a
    a += 1
```


5. turtle小海龟画图

turtle 是python很流行的绘制图像的函数库，通过x轴y轴进行坐标移动，绘制图形。
[https://www.icode9.com/content-1-808244.html](https://www.icode9.com/content-1-808244.html)

![squre](https://images.gitee.com/uploads/images/2021/1103/115336_84a7f3bf_8854469.png "屏幕截图.png")![wuhuan](https://images.gitee.com/uploads/images/2021/1103/115405_4085402e_8854469.png "屏幕截图.png")![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/115424_aaa12c8f_8854469.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1103/115431_67ec8982_8854469.png "屏幕截图.png")

# 5. 参考资料
1. 从零开始学Python [https://www.zhihu.com/column/c_1216656665569013760](https://www.zhihu.com/column/c_1216656665569013760)
2. 廖雪峰 [https://www.liaoxuefeng.com/wiki/1016959663602400](https://www.liaoxuefeng.com/wiki/1016959663602400)
3. 菜鸟教程 [https://www.runoob.com/python/python-tutorial.html](https://www.runoob.com/python/python-tutorial.html)