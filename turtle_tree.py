import random
import turtle
turtle.bgcolor("black")
tl = turtle.Pen() 
tl.speed(10)
def draw_tree(tree_length,tree_angle):
    if tree_length >= 5:
        tl.forward(tree_length)
        tl.right(tree_angle)  
        draw_tree(tree_length - 10,tree_angle)
        tl.left(2 * tree_angle)  
        draw_tree(tree_length -10,tree_angle) 
        tl.right(tree_angle)
        if tree_length <= 10:  
            tl.pencolor('yellow')
            tl.pensize(1)
        if tree_length > 30:
            tl.pencolor('green') 
            tl.pensize(4)
        if tree_length <=30 and tree_length >10:
        	tl.pencolor('grey')
        	tl.pensize(2)
        tl.backward(tree_length)  
def round( x,y,r ):
	tl.penup()
	tl.goto(x,y-r)
	tl.pendown()
	tl.setheading(0)
	tl.circle(r)
def star( x,y ):
	tl.pensize(1)
	tl.penup()
	tl.goto(x,y-3)
	tl.pendown()
	tl.begin_fill()
	tl.fillcolor('white')
	tl.setheading(0)
	tl.circle(2)
	tl.goto(x-9,y)
	tl.goto(x,y+3)
	tl.goto(x+9,y)
	tl.goto(x,y-3)
	tl.end_fill
	tl.penup()
	tl.goto(x-3,y)
	tl.pendown()
	tl.begin_fill()
	tl.fillcolor('white')
	tl.setheading(0)
	tl.goto(x,y+9)
	tl.goto(x+3,y)
	tl.goto(x,y-9)
	tl.goto(x-3,y)
	tl.end_fill()
	
	
tl.penup()
tl.left(90) 
tl.backward(250)
tl.pendown()
tree_length = 90  
tree_angle = 25  
draw_tree(tree_length,tree_angle) 

tl.pencolor('white')
tl.pensize(1)
tl.begin_fill()
tl.fillcolor('white')
round(-200,200,50)
tl.end_fill()
tl.pencolor('black')
tl.begin_fill()
tl.fillcolor('black')
round(-180,200,31)
tl.end_fill()

for x in range(0,10):
	m0 = random.randint(1, 9)
	n0 = random.randint(0, 4)
	m = 50*m0 - 100
	n = 300 - 50*n0 		
	star(m,n)
tl.hideturtle()
turtle.done()